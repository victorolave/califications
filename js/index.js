let estudiantes = [];

const getData = () => {
    fetch('/db/grades.json')
        .then(response =>
            response.json()
        )
        .then(e => {
            console.log(e);
            estudiantes = e.estudiantes;
            setList();
        });
}


const setGrades = (grades) => {



}

const setList = () => {

    let list = document.getElementById('list');

    list.innerHTML = estudiantes.map((d, index) => {
        let card = "<div class='col-md-4'>" +
            "<div class='card'>" +
            "<div class='avatar'>" +
            "<img src='img/user.png' width='130'>" +
            "</div>" +

            "<h3 class='card-title'>" + d.estudiante + "</h3>" +

            "<table class='table'>" +
            "<thead>" +
            "<tr>" +
            "<th scope='col'>#</th>" +
            "<th scope='col'>Nota</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody id='grades_"+ index +"'>"+
            "</tbody>"+
            "</table>" +
            "</div>" +
            "</div>";

        return card;
    });

    
}